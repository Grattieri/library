# -*- coding: utf-8 -*-

"""

OOP - Demo Classes :

1- Mqtt Class
2- Backbone
3- Servlet Class
4- Mongo

"""

__author__ = "Massimo Grattieri"
__copyright__ = "Massimo Grattieri"
__license__ = "mit"

import datetime
import sys
import time
import threading
import flask
import json
from flask import request
from paho.mqtt.client import Client
import pymongo
import requests


class influx:
    def insert(self, endpoint="http://localhost:8086/write?db=grafanadb",
               measurement="endpoint", value='host=server01 value=0.64'):

        res = requests.post(url=endpoint, data=measurement + ',' + value)
        print("Response", res.text)


class Mqtt:
    def __init__(self, broker_ip='127.0.0.1', broker_port=1883, publish_topic='fromPython', subscribe_topic='fromNodeRed',
                 auto_run=True):
        """
        :param broker_ip:
        :param broker_port:
        :param publish_topic:
        :param subscribe_topic:
        :param auto_run:
        """

        self.broker_ip = broker_ip
        self.broker_port = broker_port
        self.publish_topic = publish_topic
        self.subscribe_topic = subscribe_topic
        self.auto_run = auto_run
        self.client = Client()
        self.client.on_message = self.on_message
        self.client.on_connect = self.on_connect
        self.send_thread = threading.Thread(target=self.send)
        if self.auto_run:
            self.run()

    def send(self):
        while True:
            payload = str(time.time())
            topic = self.publish_topic
            print("publishing message '" + payload + "' on topic '" + topic + "'")
            self.send_message(topic=topic, payload=payload)
            time.sleep(5)

    def send_message(self, topic, payload):
        self.client.publish(topic=topic, payload=payload, qos=1, retain=False)

    def on_message(self, client, data, message):
        payload = str(message.payload.decode("utf-8"))
        topic = message.topic
        qos = message.qos
        self.on_message_response( topic, payload, qos )

    def on_message_response(self, topic, payload, qos):
        print("Received message '" + payload + "' on topic '"
              + topic + "' with QoS " + str(qos))

    def on_connect(self, client, data, flags, rc):
        print("Connected to '" + self.broker_ip + "' on port " + str(self.broker_port))
        self.client.subscribe(self.subscribe_topic)

    def run(self):
        self.client.connect(self.broker_ip, self.broker_port)
        self.client.loop_start()
        self.send_thread.start()


class Backbone:
    conf = {"dummy": 42, "ip": "192.168.200.10"}
    status = {"dummy": 42}
    buffer = {"dummy": 42}

    def __init__(self, file_name='config.json'):
        """
        :param file_name:
        """
        self.file_name = file_name

    def save(self):
        with open(self.file_name, 'w') as outfile:
            json.dump({"conf": self.conf,
                       "status": self.status,
                       "buffer": self.buffer}, outfile, separators=(',\n', ':'))

    def load(self):
        with open(self.file_name) as json_file:
            j = json.load(json_file)
            for k in j["conf"]:
                self.conf[k] = j["conf"][k]
            for k in j["status"]:
                self.status[k] = j["status"][k]
            for k in j["buffer"]:
                self.buffer[k] = j["buffer"][k]

    def print(self):
        print("================================")
        print(" File Name ", self.file_name)
        print("================================")
        print("\nconf:")
        for k in self.conf:
            print("\t", k, self.conf[k])
        print("\nstatus:")
        for k in self.status:
            print("\t", k, self.status[k])
        print("\nbuffer:")
        for k in self.buffer:
            print("\t", k, self.buffer[k])
        print("================================")


class Servlet:
    def __init__(self, endpoint='item', root="templates", default_page="index.html",
                 url='127.0.0.1', port=5000, auto_run=True):
        """
        :param endpoint:
        :param root:
        :param default_page:
        :param url:
        :param port:
        :param auto_run:
        """

        self.endpoint = endpoint
        self.root = root
        self.default_page = default_page
        self.url = url
        self.port = port
        self.auto_run = auto_run

        self.app = flask.Flask(__name__)
        self.servlet_thread = threading.Thread(target=self.app.run, args=(url, port, None, True))

        # Serve page from templates folder
        @self.app.route('/<path:path>')
        def static_pages(path):
            print("Page request")
            return flask.send_from_directory(self.root + "/", path)

        # Serve default page if none specified
        @self.app.route('/')
        def index():
            print("Default page request")
            return flask.render_template('/' + self.default_page)

        # RestApi - Create
        @self.app.route('/' + self.endpoint, methods=['POST'])
        def create():
            data = request.data
            return self.response_create(data)

        # RestApi - Read all items
        @self.app.route('/' + self.endpoint, methods=['GET'])
        def read_all():
            return self.response_read_all()

        # RestApi - Read one specific items
        @self.app.route('/' + self.endpoint + '/<string:name>', methods=['GET'])
        def read_one(name):
            return self.response_read_one(name)

        # RestApi - Update one specific item
        @self.app.route('/' + self.endpoint + '/<string:name>', methods=['PUT'])
        def update_one(name):
            data = request.data
            return self.response_update_one(name, data)

        # RestApi - Delete one specific item
        @self.app.route('/' + self.endpoint + '/<string:name>', methods=['DELETE'])
        def delete_one(name):
            return self.response_delete_one(name)

        if self.auto_run:
            self.servlet_thread.start()

    def run(self):
        self.servlet_thread.start()

    def response_create(self, data):
        print("RestApi - Create request ", data)
        j = json.loads(data.decode('UTF-8'))
        print(j)
        return {"response": 'Created'}, 201  # Created

    def response_read_all(self):
        print("RestApi - Read all request ")
        return {"response": 'list all items here'}, 200  # Ok

    def response_read_one(self, name):
        print("RestApi - Read one item ", name)
        return {"response": 'show one item here'}, 200  # Ok

    def response_update_one(self, name, data):
        print("RestApi - Update request ", name, data)
        j = json.loads(data.decode('UTF-8'))
        print(name, j)
        return {"response": 'Updated'}, 200

    def response_delete_one(self, name):
        print("RestApi - Deletion request ", name)
        return {"response": 'Deleted'}, 200


class Mongo:

    def __init__(self, db='data', collection_name='dataset', auto_run=True):
        self.auto_run = auto_run
        self.collection_name = collection_name
        self.client = pymongo.MongoClient("mongodb://localhost:33017/")
        self.db = self.client[db]
        print(self.client.list_database_names())
        self.collection = self.db[self.collection_name]
        print(self.db.list_collection_names())
        self.insert_thread = threading.Thread(target=self.auto_insert)
        if self.auto_run:
            self.insert_thread.start()

    def remove(self):
        self.collection.remove()
        print("Removed")

    def auto_insert(self):
        while True:
            self.insert({"timestamp": time.time(), "now": datetime.datetime.now().strftime("%I:%M:%S%p on %B %d, %Y")})
            time.sleep(1)

    def insert(self, data):
        res = self.collection.insert_one(data)
        print(res.inserted_id)

    def query_last(self):
        return self.collection.find().sort('_id', -1).limit(1)[0]



def main(args):
    """Main entry point allowing external calls
    """
    print("Start")
    # s = Servlet()
    # m = Mqtt()
    # b = Backbone()
    # m = Mongo()
    # m.remove()
    # s = Servlet()
    # i = influx()
    # i.insert()

def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
